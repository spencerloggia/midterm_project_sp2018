# Lines starting with # are comments

# Some variable definitions to save typing later on
CC = gcc
CONSERVATIVE_FLAGS = -std=c99 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

#Links thw project executable
project: project.o imageManip.o ppmIO.o commandUtil.o
	$(CC) -o project project.c imageManip.c ppmIO.c commandUtil.c

# Compiles test_dnasearch.c into an object file
commandUtil.o: commandUtil.c commandUtil.h ppmIO.o imageManip.o
	$(CC) $(CFLAGS) -c commandUtil.c imageManip.c ppmIO.c

# Compiles test_dnasearch.c into an object file
imageManip.o: imageManip.c imageManip.h ppmIO.o
	$(CC) $(CFLAGS) -c imageManip.c ppmIO.c

# Compiles ppmIO.c into an object file
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c



# 'make clean' will remove intermediate & executable files
clean:
	rm -f *.o project *.gcov
