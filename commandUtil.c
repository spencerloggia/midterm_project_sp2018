#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "ppmIO.h"
#include "imageManip.h"

int readInput(Image * image, const char * destname, const char * command, const char* arg1,
              const char * arg2, const char * arg3, const char* arg4, const char* arg5){
  int err = 0;
    if (command == NULL){
      return 3;
    }
    else if(0 == strncmp("swap",command, 20)){
      err = swap(image, arg1);
      if (err == 6) {
        return 6;
      }
      else if (err != 0) {
        return -1;
      }
    }
    else if(0 == strncmp("blackout",command, 20)){
      err = blackout(image, arg1);
      if (err == 6) {
        return 6;
      }
      else if (err != 0) {
        return -1;
      }
    }
    else if(0 == strncmp("grayscale",command, 20)){
      err = grayscale(image, arg1);
      if (err == 6) {
        return 6;
      }
      else if (err != 0) {
        return -1;
      }
    }
    else if(0 == strncmp("crop",command, 20)){
      if (arg1==NULL||arg2==NULL||arg3==NULL||arg4==NULL){
        return 1;
      }
      int x1 = atoi(arg1);
      int y1 = atoi(arg2);
      int x2 = atoi(arg3);
      int y2 = atoi(arg4);

      err = crop(image, x1, y1, x2, y2, arg5);
      if (err == 6 || err == 7) {
        return err;
      }
      else if (err != 0) {
        return -1;

      }
    }

    else if(0 == strncmp("contrast",command, 20)){
      if (arg1==NULL){
        return 1;
      }
      double adjust_factor = atof(arg1);
      err = contrast(image, adjust_factor, arg2);
      if (err == 6) {
        return 6;
      }
      else if (err != 0) {
        return -1;
      }
    }
    else{
    return 3;
    }
    FILE * file = createFile(destname);
    err = writePPM(file, image);
    if (err == 4) {
      return 4;
    }
    //assert (err == 0);
    return 0;
  }
