//Seunghan Baek, Spenc Loggia
//sbaek12
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "ppmIO.h"


int swap(Image* input, const char * check_next) {
  if (input == NULL || check_next != NULL) {
    return 6;
  }
  int height = input->rows;
  int width = input->cols;

  //allococate memory fore each pixel
  struct Pixel* temp_pixel = malloc(sizeof(Pixel));
  for (long i = 0; i < height*width; i++) {

    temp_pixel->g = input->data[i].b;
    temp_pixel->b = input->data[i].r;
    temp_pixel->r = input->data[i].g;

    //set input arr in heap = to temp pixel
    input->data[i] = *temp_pixel;
  }
  free(temp_pixel);
  return 0;
}

int blackout(Image* input, const char * check_next) {
  if (input == NULL || check_next != NULL) {
    return 6;
  }
  int height = input->cols;
  int width = input->rows;

  struct Pixel * pix = malloc(sizeof(Pixel));
  for(long i = 0; i < height/2; i++) {
    for (long j = width / 2; j < width; j++) {

      pix->g=0;
      pix->r=0;
      pix->b=0;

      input->data[i*width + j]= *pix;

    }
  }
  free(pix);
  return 0;
}

int crop (Image* input, int x1, int y1, int x2, int y2, const char * check_next) {
  if (check_next != NULL) {
    return 6;
  }
  //get old dimensions
  int width = input->rows;
  int height = input->cols;

  int nh = y2 - y1;
  int nw = x2 - x1;

  //make sure new dim. are in bounds
  if(width<nw || nw <= 0) return 7;
  if(height<nw || nh <= 0) return 7;

  //set new dim
  (input->rows) = nw;
  (input->cols) = nh;

  //allocate temporary pixels
  struct Pixel * pix = malloc(sizeof(Pixel));

  //loop throug the appropriat number of new rows
  for(int i = 0; i <= nh; i++) {
    //loop through appropriate number of new cols
    for (int j = 0; j <= nw; j++) {
        //pix = position relative to starting x,y coord
        *pix = input->data[(i+y1)*width+(j+x1)];
        //set input = pix relative to 0,0
        input->data[i*nw + j] = *pix;
    }
  }
   //free garbage memory
  free(pix);

  return 0;
}

int grayscale(Image* input, const char * check_next) {
  if (input == NULL || check_next != NULL) {
    return 6;
  }
  //get dimensions
  int height = input->cols;
  int width = input->rows;

  //allococate memory fore each pixel
  struct Pixel* temp_pixel = malloc(sizeof(Pixel));
  //loop all pixels
  for (long i = 0; i < height*width; i++) {
    //cast rbg to int
    int tempb= (int) input->data[i].b;
    int tempr= (int) input->data[i].r;
    int tempg= (int) input->data[i].g;

    //calculate intensity
    int intense = tempb * .11 + tempg * .59 + tempr * .3;

    //assign each component of current pixel to intesity
    temp_pixel->g = (unsigned char) intense;
    temp_pixel->b = (unsigned char) intense;
    temp_pixel->r = (unsigned char) intense;

    //set input arr in heap = to temp pixel
    input->data[i] = *temp_pixel;
  }
  free(temp_pixel);

  return 0;
}
unsigned char contrastHelper(unsigned char c, double adjust) {

  double temp = (double) c;
  temp = (temp / 255) - .5;
  temp = temp * adjust;
  temp = (temp*255) + 127;
  if (temp < 0){
    temp = 0;
  }
  if (temp > 255){
    temp = 255;
  }
  c = (unsigned char) temp;
  return c;
}

//check next used to make sure next arg is NULL
int contrast(Image* input, double adjust_factor, const char * check_next) {
  if (input == NULL || check_next != NULL) {
    return 6;
  }
  //get dimensions
  int height = input->cols;
  int width = input->rows;

  struct Pixel* temp_pixel = malloc(sizeof(Pixel));
  for (int i = 0; i < height*width; i++) {
    //allococate memory fore each pixel

    //cast rbg to int
    unsigned char tempb= input->data[i].b;
    unsigned char tempr= input->data[i].r;
    unsigned char tempg= input->data[i].g;

    //assign each component of current pixel to intesity
    temp_pixel->g = contrastHelper(tempg, adjust_factor);
    temp_pixel->b = contrastHelper(tempb, adjust_factor);
    temp_pixel->r = contrastHelper(tempr, adjust_factor);

    //set input arr in heap = to temp pixel
    *((input->data)+i) = *temp_pixel;

  }
  free(temp_pixel);
  return 0;
}
