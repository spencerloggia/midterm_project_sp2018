int swap(Image* input, const char * check_next);
int blackout(Image* input, const char * check_next);
int crop (Image* input, int x1, int x2, int y1, int y2, const char * check_next);
int grayscale(Image* input, const char * check_next);
int contrast(Image*, double adjust_factor, const char * check_next);
unsigned char contrastHelper(unsigned char c, double adjust, const char * check_next);
