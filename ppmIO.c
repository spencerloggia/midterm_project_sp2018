//ppmIO.c
//601.220, Spring 2018
//Starter code for midterm project - feel free to edit/add to this file

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "ppmIO.h"


Image* readFile(const char * filename){
    FILE * file = fopen(filename, "rb");
    if(file == NULL){
      printf("Specified input file could not be found\n");
      exit(2);
    }

    return readPPM(file);
}

FILE * createFile(const char * filename){
  FILE* file = fopen(filename, "wb");
  if (file == NULL){
    exit(4);
  }
  return file;
}

//should return a pointer to image struct
Image* readPPM(FILE *file) {
  char temp[70];
  char c;

  if(file == NULL){
    return NULL;
  }

  struct Image * image = malloc(sizeof(Image));

  //read next two chars into temp
  assert (1 == fscanf(file, "%s", temp));

   //check the image format
  if (0 != strcmp("P6", temp)) {
      return NULL;
  }
  if (!image) {
      return NULL;
  }
  //check for a comment
  fscanf(file, "\n%c", &c);
    while (c == '#') {
    while (getc(file) != '\n') {}
         c = getc(file);
    }

  //put back character if no comment
  ungetc(c, file);

  //image size to struct
  if (fscanf(file, "%d %d", &image->rows, &image->cols) != 2) {
       return NULL;
  }
  //skip reading the 255 line, max_color is assumed to be 255
  char garbage[4];
  fscanf(file, "%s", garbage);

  while (getc(file) != '\n') {}
  // m is width of matrix
  int m = image->cols;

  // n is height of matrix
  int n = image->rows;

  //allocate memory for pixel array
  image->data = malloc(sizeof(Pixel)*m*n);

  //variables to hold temporary values
  unsigned char r;
  unsigned char g;
  unsigned char b;

  //Pixel pixel_array[] = NULL;
  int count = 0;
  //loop through pseudo rows
  for(int i = 0; i < m; i++){
      //loop through pseudo columns
  		for(int z = 0; z < n; z++){

        struct Pixel * temp_pixel = malloc(sizeof(Pixel));

        //get next 9 bytes in file
        fread(&r, sizeof(char), 1, file);
      	fread(&g, sizeof(char), 1, file);
  	    fread(&b, sizeof(char), 1, file);

        //set color attributes of pixel based on file data
  		  temp_pixel->r = r;
  		  temp_pixel->g = g;
  		  temp_pixel->b = b;

        (image->data[(i*n)+z]) = *temp_pixel;
        free(temp_pixel);
        count++;
        assert(count<=(n*m));
  		}
  	}
    assert(count == m*n);

  //TODO: fill in this definition
  fclose(file);
  return image;
}

/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, Image* im) {
  /* abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
  if (!fp || !im) {
    return -1;
  }
  // m is height of matrix
  int m = im->cols;
  // n is width of matrix
  int n = im->rows;
  /* color max is always 255 */

  //get pixels from array to file
  int num_pixels_written = 0;
  fprintf(fp, "P6\n%d %d\n%d\n", n, m, 255);
  for(int i = 0; i < m*n; i++){
    int wrote = (int) fwrite((im->data+i), sizeof(Pixel), 1, fp);
    num_pixels_written++;
    if (wrote != 1){
      return -1;
    }
  }
  //free image and data array
  free(im->data);
  free(im);

    fclose(fp);
    if (num_pixels_written != m * n) {
      return -1;
    }
  /* success, so return number of pixels written */
  return num_pixels_written;
}
