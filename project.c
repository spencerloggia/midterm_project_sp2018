#include "ppmIO.h"
#include "imageManip.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "commandUtil.h"

int main(int argc, char const *argv[]) {

  assert(argc = 1);
  if (argv[1] == NULL || argv[2] == NULL) {
    printf("failed to supply Failed to supply input filename or output filename, or both\n");
    return 1;
  }
  //need return 2
  Image * image = readFile(argv[1]);
  if (image == NULL){
    printf("Specified input file is not a properly-formatted PPM file, or reading input somehow fails\n");
    return 3;
  }
  //error handling for read input
  int err = readInput(image, argv[2], argv[3], argv[4], argv[5], argv[6], argv[7], argv[8]);
  if (err == 3) {
    printf("No operation name was specified, or operation name specified was invalid\n");
    return 5;
  }
  else if (err == 1){
    printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
    return 6;
  }
  //write is called from readInout, error passed through here
  else if (err == -1) {
    printf("Specified output file could not be opened for writing, or writing output somehow fails\n");
    return 4;
  }
  if (err == 6) {
    printf("Incorrect number of arguments or kind of arguments specified for the specified operation\n");
    return 6;
  }
  if (err == 7) {
    printf("Arguments for crop operation were out of range for the given input image\n");
    return 7;
  }
  if (err != 0) {
    printf("Any other error condition not specified above\n");
    return 8;
  }
  printf("Operation successful\n");
  return 0;
}
